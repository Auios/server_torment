// data.cs

// Torment Data API - external functions

function torment_getData(%blid, %key)
{
	return $tormentData[%blid, %key];
}

function torment_setData(%blid, %key, %value)
{
	$tormentData[%blid, %key] = %value;
}

function torment_saveData(%blid)
{
	
}

function torment_loadData(%blid)
{
	
}

function torment_saveAllData()
{
	
}
