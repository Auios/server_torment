// server.cs

exec("./data.cs");
exec("./commands.cs");

function spawnBanDeletePlayer(%client)
{
    if(isObject(%client) && isObject(%client.player))
        %client.player.delete();
}

package Torment
{
	function GameConnection::spawnPlayer(%client)
	{
        if(torment_getData(%client.bl_id, "spawnBan"))
        {
            schedule(33, 0, spawnBanDeletePlayer, %client);
        }
        return parent::spawnPlayer(%client);
    }
    
    function serverCmdSuicide(%client)
    {
        if(torment_getData(%client.bl_id, "suicideBan")) return;
        parent::serverCmdSuicide(%client);
    }
    function serverCmdPlantBrick(%client)
    {
        if(torment_getData(%client.bl_id, "buildBan")) return;
        parent::serverCmdPlantBrick(%client);
    }
    
    function serverCmdCreateMinigame(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8)
    {
        if(torment_getData(%client.bl_id, "suicideBan") || torment_getData(%client.bl_id, "spawnBan")) return;
        parent::serverCmdCreateMinigame(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8);
    }
    function serverCmdJoinMinigame(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8)
    {
        if(torment_getData(%client.bl_id, "suicideBan") || torment_getData(%client.bl_id, "spawnBan")) return;
        parent::serverCmdJoinMinigame(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8);
    }
    function serverCmdAcceptMinigameInvite(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8)
    {
        if(torment_getData(%client.bl_id, "suicideBan") || torment_getData(%client.bl_id, "spawnBan")) return;
        parent::serverCmdAcceptMinigameInvite(%client, %a1, %a2, %a3, %a4, %a5, %a6, %a7, %a8);
    }
    
    function serverCmdMessageSent(%client, %msg)
    {
        if(torment_getData(%client.bl_id, "muted")) return;
        parent::serverCmdMessageSent(%client, %msg);
    }
};
schedule(20, 0, activatePackage, Torment);

package DamageAll
{
    function minigameCanDamage(%attacker, %target)
    {
        if($canDamageAll){ return 1; }
        
        if(parent::minigameCanDamage(%attacker, %target)==1){ return 1; }
        
        if(%target.getClassName()$="GameConnection" && torment_getData(%target.bl_id, "damagable"       )){ return 1; }
        if(%target.getClassName()$="Player"         && torment_getData(%target.client.bl_id, "damagable")){ return 1; }
        
        return 0;
    }
};
activatePackage(DamageAll);
schedule(1, 0, resetAllOpCallFunc);
